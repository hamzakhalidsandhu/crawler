﻿using Crawler.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler.BLL
{
    
    class BlltblUrList:IDisposable
        
    {
        private DaltblUrList table = new DaltblUrList();
        
        public BlltblUrList()
        {
            table = new DaltblUrList();
        }
        public BlltblUrList(String dbName)
        {
            table = new DaltblUrList(dbName);
        }
        
        public void CreateData(String url, String hash, String status)
        {
            table.CreateData(url,hash,status);
        }
        public void DeleteData(String del)
        {
            {
               

                //SqlConnection conn = new SqlConnection(@"Server=DESKTOP-ATODJI2\MSSQLSERVER01;Initial Catalog=crawler;Integrated Security=True");
                if (del == "one")
                {
                    Console.WriteLine("Enter the url you wish to delete");
                    String url = Console.ReadLine();
                    table.DeleteOne(url) ;

                }
                else if (del == "all")
                {
                    table.DeleteAll();

                }
            }

        }
        public void ReadTable()
        {   
            table.Readtable();
        }
        public void Update(String url, String status)
        {
            table.Update(url,status);
        }
        public void Dispose()
        {
            table.Dispose();
        }
    }
}
