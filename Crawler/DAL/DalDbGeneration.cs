﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Crawler.DAL
{
    public class DalDbGeneration:IDisposable
    {
        private SqlHelperNew sqlHelperNew = new SqlHelperNew();
        public bool CheckDbExists(string dbName)
        {

            string query = $"if db_id('{dbName}') is not null  select 'true' else select 'false'";
            DataTable dt = sqlHelperNew.FetchData(query);
            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString().Equals("true", StringComparison.OrdinalIgnoreCase);
            }

            return false;
        }
        public void CreateTblUrlList(string dbName)
        {
            string query = $"Use [{dbName}]; " +
                $"Create table tblUrlList(" +
                $"id int IDENTITY(1,1) PRIMARY KEY," +
                $"url text," +
                $"hash varchar(max)," +
                $"status varchar(20));";            
            sqlHelperNew.ExecuteNonQuery(query);
        }
        public void CreateDb(string dbName)
        {
            string query = $"Create DATABASE [{dbName}]";
            //String query = "CREATE DATABASE"+dbName;
            sqlHelperNew.ExecuteNonQuery(query);
        }

        public void Dispose()
        {
            sqlHelperNew.CloseConnection();
        }
    }
}
