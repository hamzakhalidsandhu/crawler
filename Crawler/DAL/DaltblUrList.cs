﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace Crawler.DAL
{
    class DaltblUrList : IDisposable
    {
        private SqlHelperNew sqlHelperNew; 
        SqlConnection conn = new SqlConnection(@"Server=DESKTOP-ATODJI2\MSSQLSERVER01;Initial Catalog=crawler;Integrated Security=True");
        private readonly string TableName = "tblUrlList";

        public DaltblUrList()  
        {
            sqlHelperNew = new SqlHelperNew();
        }
        public DaltblUrList(string dbName) 
        {
            sqlHelperNew = new SqlHelperNew(dbName);
        }
       
        public void CreateData(String url, String hash, String status)
        {
            String query = $"INSERT INTO {TableName}(url,hash,status) VALUES(@url,@hash,@status)";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@url", url),
                new SqlParameter("@hash", hash),
                new SqlParameter("@status", status)
            };
            sqlHelperNew.ExecuteNonQuery(query, sqlParameters);            
        }
        public void Readtable()
        {
            String query = "Select url,hash,status FROM tblUrList";
            DataTable data = new DataTable();
            data = sqlHelperNew.FetchData(query);
            for (int columns = 0; columns < data.Columns.Count; columns++)
            {
                Console.Write(data.Columns[columns].ColumnName.ToString());

                for (int rows = 0; rows < data.Rows.Count; rows++)
                {
                    Console.WriteLine(data.Rows[rows].ItemArray[columns].ToString());


                    // Console.WriteLine(data.Columns[1].ColumnName.ToString());
                    // Console.WriteLine(data.Rows[0].ItemArray[1].ToString());
                    // Console.WriteLine(data.Rows[1].ItemArray[1].ToString());
                }

            }
        }
        public void Update(String url,String status)
        {
            String query = "UPDATE tblUrList SET status=@status WHERE url=@url";
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@status",status),
                new SqlParameter("@url",url)
            };
            sqlHelperNew.ExecuteNonQuery(query, sqlParameters);
        }

        public void DeleteOne(String url)
        {
            String query = "DELETE FROM tblUrList WHERE url=@url";
            SqlParameter[] sql = new SqlParameter[]
            {
                new SqlParameter("@url",url)
            };
            sqlHelperNew.ExecuteNonQuery(query, sql);
        }
        public void DeleteAll()
        {
            String query = "DELETE FROM tblUrList";
            sqlHelperNew.ExecuteNonQuery(query);
        }


        public void Dispose()
        {
            sqlHelperNew.CloseConnection();
        }
    }
}

