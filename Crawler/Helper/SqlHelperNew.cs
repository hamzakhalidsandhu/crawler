﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crawler
{
    public class SqlHelperNew
    {
        //Server=localhost\SQLEXPRESS;Trusted_Connection=True;

        SqlConnection conn;
        //SqlConnection conn = new SqlConnection(@"Server=DESKTOP-ATODJI2\MSSQLSERVER01;Initial CatalogIntegrated Security=True");
        public SqlHelperNew()
        {
            try
            {
                conn = new SqlConnection(@"Server=DESKTOP-ATODJI2\MSSQLSERVER01;Integrated Security=True");
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    Console.WriteLine("Connection to db established successfully");
                }
                else
                {
                    Console.WriteLine("Connection to db failed");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        public SqlHelperNew(string dbName)
        {
            try
            {
                conn = new SqlConnection(@"Data Source=DESKTOP-ATODJI2\MSSQLSERVER01" + ";Initial Catalog="
                        + dbName + ";Integrated Security=True");
                conn.Open();
                if (conn.State == ConnectionState.Open)
                {
                    Console.WriteLine("Connection to db established successfully");
                }
                else
                {
                    Console.WriteLine("Connection to db failed");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void CloseConnection()
        {
            conn.Close();
            Console.WriteLine("Connection closed");
        }
        public bool ExecuteNonQuery(string query)

        {
            /*conn.Close();
            conn.ConnectionString= @"Data Source=DESKTOP-ATODJI2\MSSQLSERVER01" + ";Initial Catalog="
                        + dbName + ";Integrated Security=True";
            conn.Open();*/
            SqlCommand sqlCommand = new SqlCommand(query, conn);
            return sqlCommand.ExecuteNonQuery()>0;
            //return Convert.ToBoolean(sqlCommand.ExecuteNonQuery());
            
        }
        public DataTable FetchData(string query)
        {
            try
            {
                SqlCommand sqlCommand = new SqlCommand(query, conn);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                DataTable dt = new DataTable();
                sqlDataAdapter.Fill(dt);
                
                return dt;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
       
        public bool ExecuteNonQuery(string query, SqlParameter[] sqlParameters)
        {
            SqlCommand sql = new SqlCommand(query, conn);
            sql.Parameters.AddRange(sqlParameters);  
            sql.ExecuteNonQuery();
            return true;
        }
        

    }
}
