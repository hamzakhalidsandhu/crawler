﻿using Crawler.DAL;
using Crawler.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Crawler
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Todo:
             * 0. Add gitignore file to your code google gitignore file for c# project
             * 1. All the console statements should be in the main
             * 2. DAL means data access layer and it should only include queries and no logic
             * 3. If you want to have logic create a folder called Bll
             * 4. Bll means business logic layer where all your logic will reside like if else on the values coming from the main or from the DAL
             * 5. Look at the overloaded function that I have created in the SqlHelperNew
             * 6. If you don't know about overloaded function google it
             * 7. Delete the existing db and In the main() I have added CreateTblUrlList() which will create the table after db creation
             * 8. Call all the functions from the main like 1st call Create from tblUrlList (Take user input for url and create hash for hash value and then status pending) 
             * 9. Then use the FetchData function in the SqlHelper to fetch the data from tblUrlList (You must learn how to use that function) and display data on console. Display all values including id
             * 10. Ask user if they want to update function if yes then update the status value in db to InProgress
             * 11. Do the step 9 again and show data
             * 12. Ask the user if the want to delete the data if yes then ask the id of the data they want to delete and then delete the the row with that id in table
             * 13. Your code should not exit instead at the end of all of the above it should:
             *          i. Ask the user if they want to continue
             *         ii. If yes then it should from the start of the function again asking user to give dbName
             *        iii. It should continue with the flow without exiting
             * 14. Rename the classes in DAL folder to DalDbGeneration & DalTblUrlList
             * 15. Add the classes in Bll with name BllTblUrlList to add your logic there if there is any.
             * 16. Google what this means Server=.\\SQLEXPRESS;Initial Catalog={dbName};Integrated Security=True;           
             * 17. Create BllClasses for every Dal Class and Call only Bll classes from main (Will do later)
             * 
             * 
             * FYI
             * 1. In the SqlHelperNew class I have added another constructor which takes in dbName and will create a connection by setting the Initial Catalog value 
             * 2. This is done, so that you don't have to pass the dbName in every function or mention the dbName in every query.
             * 3. In the SqlHelperNew class the connection string you can change to your own 
            */
            String url, hash, status;
           
            Console.WriteLine("Enter the database name you wish to connect to: ");
             string dbName = Console.ReadLine(); 
             try
             {
                using (DalDbGeneration dbGeneration = new DalDbGeneration())
                 {
                     bool exists = dbGeneration.CheckDbExists(dbName);
                     if (!exists)
                     {
                         dbGeneration.CreateDb(dbName);
                         dbGeneration.CreateTblUrlList(dbName);
                         Console.WriteLine("Db created successfully");            
                        
                     }
                     else
                     {
                         Console.WriteLine("Db already exists");
                        // We can add the methods here in try 
                        using (BlltblUrList tbl=new BlltblUrList(dbName))
                        {
                            while (true)
                            { 
                                Console.WriteLine("Enter the url");
                                url = Console.ReadLine();
                                Console.WriteLine("Enter the hashkey");
                                hash = Console.ReadLine();
                                Console.WriteLine("Enter the status");
                                status = Console.ReadLine();
                                tbl.CreateData(url,hash,status);
                                tbl.ReadTable();
                                Console.WriteLine("Type yes to update the status (type:yes)");
                                String option = Console.ReadLine();
                                if (option == "Yes" || option == "yes") 
                                {
                                    Console.WriteLine("Enter the url");
                                    url = Console.ReadLine();
                                    Console.WriteLine("Change the status");
                                    status = Console.ReadLine();
                                    tbl.Update(url, status);
                                }
                                tbl.ReadTable();
                                Console.WriteLine("Do you wish to delete data?yes and no");
                                String del = Console.ReadLine();
                                if (del == "Yes" || del == "yes")
                                {
                                    Console.WriteLine("Do you wish to delete one row (type:one) or all the table data (type:all)");
                                    String inp = Console.ReadLine();
                                    tbl.DeleteData(inp);
                                }
                                Console.WriteLine("To end the flow type end (type:end)");
                                String end = Console.ReadLine();
                                if (end == "end")
                                {
                                    break;
                                }
                            }
                        }
                     }

                }


             }
             catch (Exception ex)
             {
                 Console.WriteLine($"Exection occurred with message {ex.Message}" +
                     $"\n and stack trace {ex.StackTrace}");
             }
            

            //SqlHelperNew n = new SqlHelperNew("crawler");
            //String query = "SELECT url,hash,status FROM tblUrList";
            //n.FetchData(query);
            
            Console.ReadLine();
        }
    }
}
